+++
title = "Fengfan Yang"
type = "author"

[image]
  source = "../images/Fengfan_Yang.JPG"
  copyright = "Fengfan Yang"

[website]
  url = "https://drive.google.com/file/d/1Tvuyl2_znB56ASn9BIX17xcX29E4MwZJ/view"
+++

My name is Fengfan Yang and I am an industrial design student from the State Academy of Fine Arts in Stuttgart, currently exchanged in Collaborative and Industrial Design at Aalto University.

I like to connect design to different specific contexts, so that people can enjoy the design and see the story and thinking behind the product at the same time.

+++
title = "Karl Mihhels"
type = "author"

[image]
  copyright = "Karl Mihhels"

[website]
  url = "https://karlpalo.gitlab.io/fablab2022/"
+++

I'm a chemical engineer working on my PhD in cellulose chemistry. My thesis topic is the manufacturing of nanocellulose from local seaweed called Cladophora glomerata ("Viherahdinparta in Finnish") This project is highly topical for my thesis, as the cultivation technology for indoor cultivation of algae is not very well developed. 

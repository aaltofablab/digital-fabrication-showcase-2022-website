+++
title = "Yikun Wang"
type = "author"

[image]
  copyright = "Yikun Wang"

[website]
  url = "https://yikun_wang.gitlab.io/digital-fabrication/"
+++

I am Yikun, and I am currently studying Interior Architecture at Aalto University. I have wide interests and am always seeking to learn more. For example, my interest in parametric design led me to explore digital fabrications. I love combining different fields and creating interesting projects. I'm thrilled to create my first interactive project involving sound and light at FabLab.  

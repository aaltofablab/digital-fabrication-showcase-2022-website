+++
title = "Yoona Yang"
type = "author"

[image]
  copyright = "Yoona Yang"

[website]
  url = "https://yyoona.gitlab.io/digital-fabrication/"
+++

I am a multi-disciplinary designer and media artist, studying and working in Espoo, Finland, born in the Republic of Korea. As a media artist who creates artworks with inspiration from nature, I reside at a crossroads between nature and digital media. I try to find the balance and the coexisting ambivalence - optimistic and pessimistic aspects of media technology - by observing these aspects of everyday life as a starting point for a new artistic practice. 

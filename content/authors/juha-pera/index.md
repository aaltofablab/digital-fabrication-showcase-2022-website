+++
title = "Juha Perä"
type = "author"

[image]
  copyright = "Juha Perä"

[website]
  url = "https://www.linkedin.com/in/perajuha/"
+++

Juha Perä is a designer and a musician with a special interest in interaction, media and sound design. His background is in visual arts, industrial design, UX and UI design and product development. In his artistic work Juha explores soundscapes and spatial design through his installation work and enjoys creating ambient states combining both found sound and digital and analog instruments and tools. Currently Juha is working at Futurice as a senior designer and finishing his MA studies at Aalto Media Lab in Sound in New Media.

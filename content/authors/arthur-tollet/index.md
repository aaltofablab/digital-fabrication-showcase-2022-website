+++
title = "Arthur Tollet"
type = "author"

[image]
  source = "../images/Arthur_Tollet.jpg"
  copyright = "Arthur Tollet"

[website]
  url = "https://fabacademy.org/2022/labs/aalto/students/arthur-tollet/"
+++

Master's student at Aalto University, studying space science and technology. Digital fabrication hobbyist and professional user. Participant in Fab Academy 2022

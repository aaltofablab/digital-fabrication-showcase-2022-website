+++
title = "Onni Eriksson"
type = "author"

[image]
  copyright = "Onni Eriksson"

[website]
  url = "https://onni-innovations.gitlab.io/fab-academy/"
+++

I'm an information networks student that was doing the Aalto Fablab courses in spring 2021. I’ve always loved electronics, robotics and coding. I like to spend my my school, professional and free time prototyping and making new projects whenever I find the time.  

+++
title = "Inka Jerkku"
type = "author"

[image]
  copyright = "Sini Keskitalo"

[website]
  url = "https://inkaj.gitlab.io/digital-fabrication/"
+++

Inka Jerkku is a new media master student and an artist currently focusing on 3D animation and design, (interactive) installations and audio reactive visuals. VJ:ing at different events is also part of her field of activities. Her style is often clear and colorful, creating interesting narratives and atmospheres using visual cues like symbolism, tones and abstract effects, also embracing elements like spontaneity and creative collaboration.  

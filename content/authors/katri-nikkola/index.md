+++
title = "Katri Nikkola"
type = "author"

[image]
  copyright = "Katri Nikkola"

[website]
  url = "https://www.imdb.com/name/nm10881061/"
+++

Katri Nikkola is a costume designer with experience in film, television and performing arts. As a designer she is interested in expressing and examining compelling and multifaceted characters and stories through costume. In her artistic practice she explores the boundaries of the physical forms and definitions of what costume is and what it can be. She approaches costume as a multi-sensory and interactive work of art transcending the idea of costume as clothing. Currently Katri is finishing her MA studies in Aalto University in Costume Design Major.   

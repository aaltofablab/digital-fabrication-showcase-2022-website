+++
title = "Yuhan Tseng"
type = "author"

[image]
  copyright = "Yuhan Tseng"

[website]
  url = "https://yuhantyh.gitlab.io/playground/"
+++

Hi! I am Yuhan, a New Media Design student at Aalto University. I am neither an artist nor a programmer, but I am interested in creating any interesting way that happens between humans and objects, creatures and the environment, objects and objects, and so on. That is my way to connect myself to others and the world.

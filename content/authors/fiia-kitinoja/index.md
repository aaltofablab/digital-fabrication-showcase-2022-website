+++
title = "Fiia Kitinoja"
type = "author"

[image]
  copyright = "Fiia Kitinoja"

[website]
  url = "https://digital-fabrication.fiikuna.fi/"
+++

I am a second-year master's Student studying Landscape Architecture. I have always been interested in making things one way or another, which is why I was interested in taking the Digital Fabrication courses. I had some experience in laser cutting and 3D printing beforehand but was a complete novice when it came to electronics. I have been interested in aquaponics as a sustainable way to produce food, which is why I chose it as my final project.  

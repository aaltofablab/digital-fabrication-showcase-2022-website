+++
title = "Anna Li"
type = "author"

[image]
  source = "Anna_Li.jpg"
  copyright = "Sun Weiqi"

[website]
  url = "https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/"
+++

My name is Anna Li and I'm studying Urban Studies and Planning at Aalto University. My background is in architecture: I worked in a Shanghai architecture studio for many years before coming to Finland. Most of the architecture projects were about public buildings and urban design which motivated me to think about urban issues and made me interested in urban public space design.  

I'm also interested in building things, for example, furniture and other products. Most of them are focused on the user: currently, this user is my cat. I have two sweet cats, they are 7 years old. A lot of things were inspired by my cats. I made an indoor wooden bench for both me and my cats last year. It really improved our well-being last winter in Finland. I also made wooden cat toys, but they don’t always like them that much, they mostly ignore them. User perspective design motivated me to explore animal behavior in the future.

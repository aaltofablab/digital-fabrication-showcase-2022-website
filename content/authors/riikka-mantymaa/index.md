+++
title = "Riikka Mäntymaa"
type = "author"

[image]
  copyright = "Lassi Karhulahti"

[website]
  url = "https://riikkamantymaa.portfolio.site/"
+++

As a costume designer I am interested in the points where the body, space and environment meet and exploring them through costume. At the center of my work are the movement of the costume, the dialogue between the costume and the surrounding space, and their importance for the performer's work.

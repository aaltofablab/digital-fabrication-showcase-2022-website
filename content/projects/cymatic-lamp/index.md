+++
title = "Cymatic Lamp"
authors = ["yikun-wang"]
type = "project"

[images]
copyright = "Yikun Wang"
+++

The idea originally started with researching caustic light effects. I would like to create an atmospheric lamp that projects dynamic light waves on the wall. Then I learned about cymatics and was fascinated with this phenomenon. So, I started to think about combining sound into the design, using the sound wave to vibrate water and generate patterns for the light. In the end, the project is a combination of sound, water, and light. The project includes a water lamp and a touch controller. The fabrication method includes 3D printing, laser cutting, and CNC milling for PCB, It is designed for people to explore the cymatic phenomenon themselves and have fun with sound and patterns! 

+++
title = "Led Vest MK3"
authors = ["arthur-tollet"]
type = "project"

[images]
copyright = "Arthur Tollet"
+++

Student band vest with wearable LED integration. Third version incorporates modular, flexible circuit board mounted, individually addressable RGB LEDs. LEDs have multiple programs, all run from a pocket mounted custom microcontrolled module.

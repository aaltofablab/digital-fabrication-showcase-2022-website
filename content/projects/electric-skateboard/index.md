+++
title = "Electric Skateboard"
authors = ["onni-eriksson"]
type = "project"

[images]
copyright = "Onni Eriksson"
+++

I wanted to build an electric longboard as the final project for the Digital Fabrication Studio course.

I started the project by watching many YouTube videos about people building their own boards and looking at different forums, for example https://electric-skateboard.builders/. This was a particularly useful and good forum to get information and help in. It seemed like a cool project, so I gathered a parts list and ordered some of the parts myself and some through my local teacher.

For the final design, I bought a spot welder for the batteries and made the battery pack in 2 diverse ways. It turned out that the only way the skateboard battery would be safe from the ground was to be flat and long. After redesigning and adding aluminium U-piece reinforcements the new deck with the new box was able to hold even my weight well! If I did any more fixes on this project, I would switch the motors to versions which can tell the phase of the motor.  

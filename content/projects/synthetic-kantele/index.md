+++
title = "Synthetic Kantele & Audioreactive Visuals"
authors = ["inka-jerkku"]
type = "project"

[images]
copyright = "Sini Keskitalo"
+++

“Waves” is an audiovisual installation where you can play a synthetic version of a kantele, a traditional Finnish instrument, which triggers abstract audio reactive visuals. Based on the digital fabrication course project plan, the first prototype kantele was made in the Aalto Fablab by laser cutting acrylic and 3D printing. Another version was made with the CNC machine by milling the container from wood. With no previous experience from instrument making, the project was an experiment to find out what it takes to make a functioning kantele. With trial and error, the plan was to make a demo version that then could be improved further.

The visuals react differently to low, middle and high notes, particles appearing and disappearing controlled by the sound data from a pickup microphone attached to the instrument. This interaction might encourage people to try to play the instrument in different ways: how you play is not just based on what you hear but also on what you see. For some people, seeing music in a visual representation can help to experience sounds in a new way, when the focus shifts from pure sound to the images that are synced and defined by it. 

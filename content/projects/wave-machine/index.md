+++
title = "Wave Machine"
authors = ["yuhan-tseng"]
type = "project"

[images]
copyright = "Yuhan Tseng"
+++

This is a machine that is made up of 16 up-down units. There is no deep meaning to it, the main reason it exists is that I like the movement and the sound of mechanical structures. With different programs and sensors, it can interact with the different input information. In this version, the machine interacts with the music that is chosen by the audience, exploring another way to display the music and beats. The movement of the machine and those little creatures try to create a feeling between order and chaos. 

This project is good practice for me to apply what I learned from Digital Fabrication Course last semester. Most of the parts are made in Aalto Fablab, and it was nice to use the EDM technic to create the metal parts of this machine. In terms of the hardware to the software, this machine is made by PCB designing and making, CAD design and cutting, the communication of signal, 3D modelling and printing, and mechanic construction design. Sometimes the process is more valuable than the outcome : )

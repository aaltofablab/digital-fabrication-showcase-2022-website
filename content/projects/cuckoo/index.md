+++
title = "Cuckoo"
authors = ["fengfan-yang"]
type = "project"

[images]
copyright = "Fengfan Yang"
+++

"Cuckoo" is a clock designed to help people remember time better and to make time more perceptible.  

In fast paced life, we often look at the clock and shortly afterwards have already forgotten the time. It also happens to me quite often. Conventional clocks show the time very clearly, and perhaps therefore we can't remember the time even after we have looked at it.

The act of looking at the time is so easy that many people habitually don't pay attention to it. So what if we did the opposite and made the cost of knowing time higher?

"Cuckoo" is not a practical clock, because it is difficult to read. The clock face constantly swings from left to right and disappears behind the rectangular cover. Looking at the clock becomes more time-consuming and thus more conscious.

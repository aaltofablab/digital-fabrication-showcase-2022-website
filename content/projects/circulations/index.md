+++
title = "Circulations"
authors = ["yoona-yang"]
type = "project"

[images]
copyright = "Yoona Yang"
+++

The project shows the kinetic installation as the outcome. It is a part of the artistic practice, exploring the idea of coexisting ambivalence.

Each wooden plate switches the two sides by rotating, and the two wooden plates are in circulation. The circulation happens according to the moment that the two plates meet each other. The moments make a natural and ambient sound and show the communication between the two plates. If there is only one plate, the moments do not happen. The sound reminds us of the moment that the objects are connected so that they can also do their own acts. 

This project went through the digital fabrication process, including 3D printing, Laser-cutting, and embedded programming. By inputting data using the magnet and the Hall Effect sensor, the stepping motors as the output devices, find the moment that starts rotating the wooden plates.

+++
title = "Book of Rooms"
authors = ["riikka-mantymaa"]
type = "project"

[images]
copyright = "Jesper Dolgov and Lassi Karhulahti"
+++

An old factory building and its courtyard in Turku was transformed into a multi-sensory immersive experience Huoneiden kirja (The Book of Rooms), based on Saila Susiluoto’s poem anthology. The 64 poems, or rooms, were reimagined by the artists into performances, installations or other works of art.

Upon arriving, the audience threw a coin in a ritual led by the masked character Vanha Taulukko (Old Chart). The audience received their own personal poem, which they were encouraged to find at their own pace. The building was supervised by a horde of green-robed servants called Hovimestari (The Butler).  

The costume and space design factored in the two levels of reality in the performance, the fantastic level of the house and the reality outside and around the performance. To achieve a weirdly timeless yet futuristic look, laser engravings were used on the masks and green velvet vests as if the characters were branded by the house and the poems within it. The motifs were inspired by the symbols from the physical copy of the poem anthology paired with the performance’s logo. All engravings were done with the Aalto Fablab laser cutter.

Photography by Jesper Dolgov and Lassi Karhulahti.

Costume design, costume dramaturgy and laser engraving Riikka Mäntymaa

Graphic Designer Jenni Sormunen (Huoneiden Kirja logo)

Huoneiden Kirja trailer shot and edited by Jussi Virkkunen, music Hilla Väyrynen

Full credits of Huoneiden Kirja: https://huoneidenkirja.fi/taiteilijat

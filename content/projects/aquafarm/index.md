+++
title = "aquaFarm"
authors = ["fiia-kitinoja"]
type = "project"

[images]
copyright = "Fiia Kitinoja"
+++

aquaFarm is a smart aquaponics system that fits easily in an indoor apartment setting. It consists of a wooden planter box full of expanded clay pebble grow medium and a growlight, 160 L aquarium, and a custom welded and painted steel frame holding it all together. The electronics turn the control the aquarium light and the growlight, and turn them on and off according to the timer. Sensors, which include a water temperature sensor, room temperature sensor, pH sensor, and a water level sensor help monitor the water parameters to ensure the balance of the ecosystem. The basic idea of an aquaponics system is that it is a closed ecosystem, where the water circulates between the growbed full of plants, and the aquarium full of fish. The bacteria growing in the growing medium will break the ammonia from the fish waste into nitrites and nitrates, which will be absorbed by the plants as nutrients. 

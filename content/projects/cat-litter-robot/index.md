+++
title = "Cat Litter Robot"
authors = ["anna-li"]
type = "project"

[images]
copyright = "Anna Li"
+++

Every cat owner has experience cleaning poop. I have lived with my cats for 7 years and it became my job. But when I was traveling, I really hoped there would be a cat litter robot to clean automatically. This motivated me to make this project. There are two ideas for the basis, one is safe for cats, and the other is functionally used, with no stuck, no electronic bug…  

This main cat litter robot prototype is built in Fablab. The whole package includes electronic design, interface design, model making, and some webpage design. The main structure and electronic boards are made in Fablab, and the metal parts are made in the metal workshop in Väre. It was built by machines: 3D printers, laser cutter, and waterjet machines.  

The liner movement is driven by one motor driver, and the rotating of the scoop is driven by two servo motors. One weight sensor could detect if the cat comes in the box, which means it would send a message to the motor driver when it detected a weight over 5kg (each cat weighs over 5kg). Two effect sensors will detect the moving liner track before they touch the edge of the box.   

In the future, the cat litter box could collect graphic data on cat poop, which helps cat owners check if their cat is healthy. If you decide to live with cats for a long time and improve their well-being, one cat litter robot is needed.

+++
title = "Algae Growth System"
authors = ["karl-mihhels"]
type = "project"

[images]
copyright = "Karl Mihhels"
+++

The Algae growth system is a device for cultivating algae in different conditions. The system allows for 5 simultaneous experiments, with control of pH, nutrients and water movement for each tank and light/temperature control shared between the system.

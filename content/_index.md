+++
title = "Welcome to Digital Fabrication Showcase 2022!"
lead = "Aalto Fablab celebrates a year of creativity with projects made in or with the Aalto Fablab, Aalto Studios and other Aalto University workshops."
+++

Digital Fabrication Showcase is an annual show where we present projects made with or in Aalto Fablab, Aalto Studios. It started in 2021 with the 10th anniversary of Aalto Fablab and proved to be a great platform to show the variety of ideas circulating at Aalto University.

Most of the 2022 showcase works are final projects of the Fab Academy and Digital Fabrication courses (AXM-E7009, AXM-E7010, AXM-E7011). Still, a fab lab is open to all departments of Aalto University, and a variety of different projects emerge from other contexts.

You are welcome to join the exhibition opening on 7 November at 17:00 in Väre main lobby. The show will be open until 19 November 2022.
